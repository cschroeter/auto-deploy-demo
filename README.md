# The EdomoSystems Website


## How to run the application
```
docker build -t edomo-website .
docker run -d -p 80:80 -p 443:443 edomo-website
```


## How to deploy to AWS
Generate the login and execute what you got:
```
aws ecr get-login (provides you the next command)
docker login -u AWS -p SOMETHING VERY LONG
```

Build the docker image, tag it and push to our repository
```
docker build -t edomo-website .
docker tag edomo-website:latest 859897537304.dkr.ecr.eu-central-1.amazonaws.com/edomo-website:latest
docker push 859897537304.dkr.ecr.eu-central-1.amazonaws.com/edomo-website:latest
```



## first use tasks with AWS & docker
First [install docker](https://docs.docker.com/engine/installation/#platform-support-matrix) - there's versions for every os, then
[Install AWS-CLI](http://docs.aws.amazon.com/cli/latest/userguide/installing.html)

Create 'Security Credentials' -> 'Acces-Keys' for your user [IAM->USER](https://console.aws.amazon.com/iam/home?region=eu-central-1#/users) on and configure it locally:
`aws configure` so _aws-cli_ knows where to publish your images and your credentials.
```
AWS Access Key ID [****************WYBQ]:
AWS Secret Access Key [****************9M8y]:
Default region name [eu-central-1]:
Default output format [None]:
```

....[or believe in what Amazon says :-)](http://docs.aws.amazon.com/AmazonECS/latest/developerguide/get-set-up-for-amazon-ecs.html)


## Setup SSL-Certificates with letsencrypt
Run `certbot certonly --manual` on your local machine and follow the given steps.
After this, the generated SSL-Files are located in the `/etc/letsencrypt/archive/` directory. This directory contains all for the encryption necessary files. Copy those into the projects-directory `ssl/<DOMAIN>`.
To use the certificate, it must be configured in `nginx.conf` as its already done for other domains.

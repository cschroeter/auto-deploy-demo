FROM nginx:stable-alpine

COPY src/ /usr/share/nginx/html
COPY nginx/ssl/ /etc/ssl/
COPY nginx/nginx.conf /etc/nginx/nginx.conf

EXPOSE 80 443
